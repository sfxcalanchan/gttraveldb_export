#!/bin/sh

DATE=`date +%Y-%m-%d`
sudo chmod -Rf 777 opt/backups/

echo "mongoexport - location Collection ... "
sudo mongoexport --db gttraveldb --collection location --type=csv --fields _id,city.displayName,country.displayName,author.full_name,title1,title2,content,address,opening,hours,transport, --out location_$DATE.csv

echo "mongoexport - counrty Collection ... "
sudo mongoexport --db gttraveldb --collection country --type=csv --fields _id,name,ex_from_name, --out countries_$DATE.csv

echo "mongoexport - city Collection ... "
sudo mongoexport --db gttraveldb --collection city --type=csv --fields _id,cc,displayName --out cities_$DATE.csv


